# The Fastest Mirror Finder -  Fedora 

## Fedora - <b> Mirror Finder </b> / Version -1.00 
   
  
### Note : 
This application <b> Mirror Finder </b> Searches for the fastest mirrors available for fedora and RPM fusion repos and saves them in clients repo files and links. This application will test for the top nearest 100 fedora mirros and 40 RPM fusion mirros to find th best nearest one.


### Built with : 
- Library Web Qt
- Library qt-base
- qt5-qtbase-level

##### Prerequisites
- Repositor Repo detail
- Fedora Main repository
- RPM fusion RPMFusion free and RPMFusion non-free

### Getting started
 To get best out of this better to take a back up of current repos and files.
 see the how to install and compile to proceed.


### How to Compile:
You just need to make MirrorFinder executable using command :
`make` </p>

#### To run:
- <b>Run once and feel it..</b>

<p>Dont Forget To Make It Executable</p>
- `sudo./MirroFinder` </p>

### Authors
- `A.Baloochi` </p>
- `P. Santhosh` </p>
- `A. xyz` </p>


#### License
This Project is Licensed under the MIT License - see the website for details



           ==========================================
<b>contact info :</b>

`Santchampion07@gmail.com`
</p>

